package com.inkglobal.techtest.impl;


import com.inkglobal.techtest.HoursRepresentation;
import com.inkglobal.techtest.util.TimeData;
import org.joda.time.Hours;
import static com.inkglobal.techtest.util.TimeData.*;

import java.util.Arrays;

public class BerlinClockHoursImpl implements HoursRepresentation {

    @Override
    public TimeData getHoursRepresentation(Hours hours) {
        String [] fiveHours = new String[FIRST_ROW_HOURS_LENGTH];
        Arrays.fill(fiveHours, OFF_VALUE);
        String [] oneHours = new String[SECOND_ROW_HOURS_LENGTH];
        Arrays.fill(oneHours, OFF_VALUE);

        for (int i = 0; i < hours.dividedBy(MAX_HOURS).getHours(); i++){
            fiveHours[i] = RED;
        }
        for (int j = 0; j < hours.getHours() % MAX_HOURS; j++){
            oneHours[j] = RED;
        }
        return new TimeData(fiveHours, oneHours);
    }
}
