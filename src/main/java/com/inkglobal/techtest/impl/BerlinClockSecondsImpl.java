package com.inkglobal.techtest.impl;


import com.inkglobal.techtest.SecondsRepresentation;
import com.inkglobal.techtest.util.TimeData;
import org.joda.time.Seconds;

import static com.inkglobal.techtest.util.TimeData.MAX_SECONDS;
import static com.inkglobal.techtest.util.TimeData.OFF_VALUE;
import static com.inkglobal.techtest.util.TimeData.YELLOW;

public class BerlinClockSecondsImpl implements SecondsRepresentation{

    @Override
    public TimeData getSecondsRepresentation(Seconds seconds) {
        String [] secondsOnOff = new String[1];
        secondsOnOff [0] = (seconds.getSeconds() % MAX_SECONDS == 0)? YELLOW : OFF_VALUE;
        return new TimeData(secondsOnOff, null);
    }
}
