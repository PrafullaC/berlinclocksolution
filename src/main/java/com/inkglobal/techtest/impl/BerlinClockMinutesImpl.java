package com.inkglobal.techtest.impl;

import com.inkglobal.techtest.MinutesRepresentation;
import com.inkglobal.techtest.util.TimeData;
import org.joda.time.Minutes;

import java.util.Arrays;

import static com.inkglobal.techtest.util.TimeData.*;

public class BerlinClockMinutesImpl implements MinutesRepresentation {

    @Override
    public TimeData getMinutesRepresentation(Minutes minutes) {
        String [] fiveMinutesRow = new String[FIRST_ROW_MINUTES_LENGTH];
        Arrays.fill(fiveMinutesRow, OFF_VALUE);
        String [] oneMinutesRow = new String[SECOND_ROW_MINUTES_LENGTH];
        Arrays.fill(oneMinutesRow, OFF_VALUE);

        for (int i = 0; i < minutes.dividedBy(MAX_MINUTES).getMinutes(); i++){
            fiveMinutesRow[i] = (((((i + 1) * MAX_MINUTES) % MINUTES_QUARTER) == 0) ? RED : YELLOW); // i == 5 minutes
        }
        for (int j = 0; j < minutes.getMinutes() % MAX_MINUTES; j++){
            oneMinutesRow[j] = YELLOW;
        }
        return new TimeData(fiveMinutesRow, oneMinutesRow);
    }
}
