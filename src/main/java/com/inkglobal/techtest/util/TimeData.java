package com.inkglobal.techtest.util;


public class TimeData {

    public static final String RED = "R";
    public static final String YELLOW = "Y";
    public static final int MAX_HOURS = 5;
    public static final int MAX_MINUTES = 5;
    public static final int MAX_SECONDS = 2;
    public static final int MINUTES_QUARTER = 15;
    public static final int FIRST_ROW_HOURS_LENGTH = 4;
    public static final int SECOND_ROW_HOURS_LENGTH = 4;
    public static final String OFF_VALUE = "O";

    public static final int FIRST_ROW_MINUTES_LENGTH = 11;
    public static final int SECOND_ROW_MINUTES_LENGTH = 4;


    private final String [] firstRow;
    private final String [] secondRow;

    public TimeData(String[] firstRow, String[] secondRow){
        this.firstRow = firstRow;
        this.secondRow = secondRow;
    }

    public String[] getFirstRow() {
        return firstRow;
    }

    public String[] getSecondRow() {
        return secondRow;
    }

    @Override
    public String toString(){
      StringBuilder builder = new StringBuilder();
      for (String firstRowElement:firstRow){
          builder.append(firstRowElement);
      }
      builder.append("\n");
      if (null != secondRow) {
          for (String secondRowElement:secondRow){
              builder.append(secondRowElement);
          }
          builder.append("\n");
      }
      return builder.toString();
    }
}
