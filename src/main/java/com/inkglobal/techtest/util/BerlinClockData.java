package com.inkglobal.techtest.util;


public class BerlinClockData {

    private final TimeData hoursData;
    private final TimeData minutesData;
    private final TimeData secondsData;

    public BerlinClockData (final TimeData hoursData,
                            final TimeData minutesData,
                            final TimeData secondsData){
        this.hoursData = hoursData;
        this.minutesData = minutesData;
        this.secondsData = secondsData;
    }

    @Override
    public String toString(){
       StringBuilder builder = new StringBuilder();
       builder.append(secondsData.toString());
       builder.append(hoursData.toString());
       builder.append(minutesData.toString());
       return builder.toString();
    }
}
