package com.inkglobal.techtest;

import com.google.inject.Inject;
import com.inkglobal.techtest.util.BerlinClockData;
import com.inkglobal.techtest.util.TimeData;
import org.joda.time.Hours;
import org.joda.time.LocalTime;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

public class BerlinClock {

    private final HoursRepresentation hoursRepresentation;
    private final MinutesRepresentation minutesRepresentation;
    private final SecondsRepresentation secondsRepresentation;

    @Inject
    public BerlinClock(final HoursRepresentation hoursRepresentation,
                       final MinutesRepresentation minutesRepresentation,
                       final SecondsRepresentation secondsRepresentation
                       ){
       this.hoursRepresentation = hoursRepresentation;
       this.minutesRepresentation = minutesRepresentation;
       this.secondsRepresentation = secondsRepresentation;
    }

    public BerlinClockData getBerlinClockRepresentation (LocalTime localTime){
        TimeData hoursData = this.hoursRepresentation.getHoursRepresentation(Hours.hours(localTime.getHourOfDay()));
        TimeData minutesData = this.minutesRepresentation.getMinutesRepresentation(Minutes.minutes(localTime.getMinuteOfHour()));
        TimeData secondsData = this.secondsRepresentation.getSecondsRepresentation(Seconds.seconds(localTime.getSecondOfMinute()));
        return new BerlinClockData(hoursData, minutesData, secondsData);
    }

}
