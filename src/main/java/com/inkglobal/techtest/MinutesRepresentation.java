package com.inkglobal.techtest;


import com.inkglobal.techtest.util.TimeData;
import org.joda.time.Minutes;

public interface MinutesRepresentation {

    public TimeData getMinutesRepresentation(Minutes minutes);
}
