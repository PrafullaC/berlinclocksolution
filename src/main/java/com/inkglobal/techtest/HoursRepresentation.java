package com.inkglobal.techtest;

import com.inkglobal.techtest.util.TimeData;
import org.joda.time.Hours;

public interface HoursRepresentation {

    public TimeData getHoursRepresentation(Hours hours);

}
