package com.inkglobal.techtest;


import com.inkglobal.techtest.util.TimeData;
import org.joda.time.Seconds;

public interface SecondsRepresentation {

    public TimeData getSecondsRepresentation(Seconds seconds);

}
