package com.inkglobal.techtest;


import com.google.inject.AbstractModule;
import com.inkglobal.techtest.impl.BerlinClockHoursImpl;
import com.inkglobal.techtest.impl.BerlinClockMinutesImpl;
import com.inkglobal.techtest.impl.BerlinClockSecondsImpl;

public class BerlinClockTestModule extends AbstractModule{

    @Override
    protected void configure() {
        bind(HoursRepresentation.class).to(BerlinClockHoursImpl.class);
        bind(MinutesRepresentation.class).to(BerlinClockMinutesImpl.class);
        bind(SecondsRepresentation.class).to(BerlinClockSecondsImpl.class);
    }
}
