package com.inkglobal.techtest;


import com.inkglobal.techtest.impl.BerlinClockHoursImpl;
import com.inkglobal.techtest.util.TimeData;
import org.joda.time.Hours;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class BerlinClockHoursTest {

    private HoursRepresentation berlinClockHours;
    private static String OFF = "O";

    @Before
    public void setUp(){
        berlinClockHours = new BerlinClockHoursImpl();
    }

    @Test
    public void testHourZero() {
        String [] fiveHoursRow = {OFF, OFF, OFF, OFF};
        String [] oneHourRow = {OFF, OFF, OFF, OFF};
        TimeData timeData = berlinClockHours.getHoursRepresentation(Hours.hours(0));
        assertArrayEquals(fiveHoursRow, timeData.getFirstRow());
        assertArrayEquals(oneHourRow, timeData.getSecondRow());
    }

    @Test
    public void testHourMorning() {
        String [] fiveHoursRow = {OFF, OFF, OFF, OFF};
        String [] oneHourRow = {"R", OFF, OFF, OFF};
        TimeData timeData = berlinClockHours.getHoursRepresentation(Hours.hours(1));
        assertArrayEquals(fiveHoursRow, timeData.getFirstRow());
        assertArrayEquals(oneHourRow, timeData.getSecondRow());
    }

    @Test
    public void testHourAfternoon() {
        String [] fiveHoursRow = {"R", "R", OFF, OFF};
        String [] oneHourRow = {"R", "R", "R", "R"};
        TimeData timeData = berlinClockHours.getHoursRepresentation(Hours.hours(14));
        assertArrayEquals(fiveHoursRow, timeData.getFirstRow());
        assertArrayEquals(oneHourRow, timeData.getSecondRow());
    }

    @Test
    public void testHourEvening() {
        String [] fiveHoursRow = {"R", "R", "R", "R"};
        String [] oneHourRow = {"R", "R", "R", OFF};
        TimeData timeData = berlinClockHours.getHoursRepresentation(Hours.hours(23));
        assertArrayEquals(fiveHoursRow, timeData.getFirstRow());
        assertArrayEquals(oneHourRow, timeData.getSecondRow());
    }
}
