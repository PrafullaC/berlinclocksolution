package com.inkglobal.techtest;


import com.inkglobal.techtest.impl.BerlinClockMinutesImpl;
import com.inkglobal.techtest.util.TimeData;
import org.joda.time.Minutes;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class BerlinClockMinutesTest {

    private MinutesRepresentation berlinClockMinutes;
    private static String OFF = "O";

    @Before
    public void setUp(){
        berlinClockMinutes = new BerlinClockMinutesImpl();
    }

    @Test
    public void testMinutesZero(){
        String [] fiveMinutesRow = {OFF, OFF, OFF, OFF,OFF, OFF, OFF, OFF,OFF,OFF,OFF};
        String [] oneMinuteRow = {OFF, OFF, OFF, OFF};
        TimeData timeData = berlinClockMinutes.getMinutesRepresentation(Minutes.minutes(0));
        assertArrayEquals(fiveMinutesRow, timeData.getFirstRow());
        assertArrayEquals(oneMinuteRow, timeData.getSecondRow());
    }

    @Test
    public void testMinutesFirstQuarter(){
        String [] fiveMinutesRow = {"Y", "Y", "R", OFF,OFF, OFF, OFF, OFF,OFF,OFF,OFF};
        String [] oneMinuteRow = {"Y", OFF, OFF, OFF};
        TimeData timeData = berlinClockMinutes.getMinutesRepresentation(Minutes.minutes(16));
        assertArrayEquals(fiveMinutesRow, timeData.getFirstRow());
        assertArrayEquals(oneMinuteRow, timeData.getSecondRow());
    }

    @Test
    public void testMinutesSecondQuarter(){
        String [] fiveMinutesRow = {"Y", "Y", "R", "Y","Y", "R", OFF, OFF,OFF,OFF,OFF};
        String [] oneMinuteRow = {"Y", "Y", OFF, OFF};
        TimeData timeData = berlinClockMinutes.getMinutesRepresentation(Minutes.minutes(32));
        assertArrayEquals(fiveMinutesRow, timeData.getFirstRow());
        assertArrayEquals(oneMinuteRow, timeData.getSecondRow());
    }

    @Test
    public void testMinutesThirdQuarter(){
        String [] fiveMinutesRow = {"Y", "Y", "R", "Y","Y", "R", "Y", "Y","R",OFF,OFF};
        String [] oneMinuteRow = {"Y", "Y", "Y", OFF};
        TimeData timeData = berlinClockMinutes.getMinutesRepresentation(Minutes.minutes(48));
        assertArrayEquals(fiveMinutesRow, timeData.getFirstRow());
        assertArrayEquals(oneMinuteRow, timeData.getSecondRow());
    }

    @Test
    public void testMinutesEdgeCase(){
        String [] fiveMinutesRow = {"Y", "Y", "R", "Y","Y", "R", "Y", "Y","R","Y","Y"};
        String [] oneMinuteRow = {"Y", "Y", "Y", "Y"};
        TimeData timeData = berlinClockMinutes.getMinutesRepresentation(Minutes.minutes(59));
        assertArrayEquals(fiveMinutesRow, timeData.getFirstRow());
        assertArrayEquals(oneMinuteRow, timeData.getSecondRow());
    }
}
