package com.inkglobal.techtest;


import com.inkglobal.techtest.impl.BerlinClockSecondsImpl;
import com.inkglobal.techtest.util.TimeData;
import org.joda.time.Seconds;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class BerlinClockSecondsTest {

    private SecondsRepresentation secondsRepresentation;
    private static String OFF = "O";

    @Before
    public void setUp(){
        secondsRepresentation = new BerlinClockSecondsImpl();
    }

    @Test
    public void testSecondsOff (){
        String [] seconds = {OFF};
        TimeData timeData = secondsRepresentation.getSecondsRepresentation(Seconds.seconds(1));
        assertArrayEquals (seconds, timeData.getFirstRow());
    }

    @Test
    public void testSecondsOn (){
        String [] seconds = {"Y"};
        TimeData timeData = secondsRepresentation.getSecondsRepresentation(Seconds.seconds(16));
        assertArrayEquals (seconds, timeData.getFirstRow());
    }
}
