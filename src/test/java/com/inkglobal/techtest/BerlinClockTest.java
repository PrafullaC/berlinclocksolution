package com.inkglobal.techtest;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.inkglobal.techtest.util.BerlinClockData;
import org.joda.time.LocalTime;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class BerlinClockTest {

    private BerlinClock berlinClock;
    private static Injector injector;

    @BeforeClass
    public static void init(){
       injector = Guice.createInjector(new BerlinClockTestModule());
    }

    @Before
    public void setUp(){
       berlinClock = injector.getInstance(BerlinClock.class);
    }


    @Test
    public void testCase1 (){
        StringBuilder builder = new StringBuilder();
        builder.append("Y").append("\n").append("OOOO").append("\n").append("OOOO").append("\n").append("OOOOOOOOOOO").append("\n").append("OOOO").append("\n");

        BerlinClockData berlinClockData = berlinClock.getBerlinClockRepresentation(new LocalTime("00:00:00"));
        assertEquals(builder.toString(), berlinClockData.toString());
    }

    @Test
    public void testCase2 (){
        StringBuilder builder = new StringBuilder();
        builder.append("O").append("\n").append("RROO").append("\n").append("RRRO").append("\n").append("YYROOOOOOOO").append("\n").append("YYOO").append("\n");

        BerlinClockData berlinClockData = berlinClock.getBerlinClockRepresentation(new LocalTime("13:17:01"));
        assertEquals(builder.toString(), berlinClockData.toString());
    }

    @Test
    public void testCase3 (){
        StringBuilder builder = new StringBuilder();
        builder.append("O").append("\n").append("RRRR").append("\n").append("RRRO").append("\n").append("YYRYYRYYRYY").append("\n").append("YYYY").append("\n");

        BerlinClockData berlinClockData = berlinClock.getBerlinClockRepresentation(new LocalTime("23:59:59"));
        assertEquals(builder.toString(), berlinClockData.toString());
    }

    // TODO : Remove this test if the data is regarded as invalid after discussion.
    @Test
    @Ignore("Not a valid test data")
    public void testCase4 (){
        StringBuilder builder = new StringBuilder();
        builder.append("Y").append("\n").append("RRRR").append("\n").append("RRRR").append("\n").append("OOOOOOOOOOO").append("\n").append("OOOO").append("\n");

        BerlinClockData berlinClockData = berlinClock.getBerlinClockRepresentation(new LocalTime("24:00:00"));
        assertEquals(builder.toString(), berlinClockData.toString());
    }
}
